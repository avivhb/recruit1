@extends('layouts.app')

@section('title','interviews')

@section('content');
    <!-- create the ref that direct us to the create page -->

    <div><a href = "{{url('/interviews/create')}}">Add New Interview</a></div>

    <h1>List Of Interviews</h1>
    <table class="table table-dark">
        <tr>
            <th>Id</th><th>Text</th><th>Candidate</th><th>User</th><th>Created</th><th>Updated</th>
        </tr>
        @foreach($interviews as $interview)
            <tr>
                <td>{{$interview->id}}</td>
                <td>{{$interview->text}}</td>
                <td>
                @if(isset($interview->candidate_id))
                {{$interview->owner->name}}
                @endif
                </td>
                <td>
                @if(isset($interview->user_id))
                {{$interview->interviewer->name}}
                @endif
                </td>
                <td>{{$interview->created_at}}</td>
                <td>{{$interview->updated_at}}</td>
            </tr>
        @endforeach
    </table>

@endsection;
