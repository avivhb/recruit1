<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = ['text','candidate_id'];

    public function owner(){
        return $this->belongsTo('App\Candidate','candidate_id');
    }

    public function interviewer(){
        return $this->belongsTo('App\User','user_id');
    }
}
