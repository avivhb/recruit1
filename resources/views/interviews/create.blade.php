@extends('layouts.app')

@section('title','Create Candidate')

@section('content');

    <h1>Create Interview</h1>
    <!-- php artisan route:list -->
    <form method = "post" action ="{{action('InterviewsController@store')}}">
        @csrf <!-- Security -->
        <div class="form-group">
            <label for = "text">Text</label>
            <input type = "text" class="form-control" name = "text"> 
        </div>
        <div>


        <td> <!-- candidate DROPDOWN -->
        <div class="form-group row">
                            <label for="candidate_id" class="col-md-4 col-form-label text-md-right">{{_('Candidate') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" name="candidate_id">
                                    @foreach($candidates as $candidate)
                                        <option value="{{$candidate->id}}">
                                            {{$candidate->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

        </td>
        <td> <!-- user DROPDOWN -->
        <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">{{_('User') }}</label>
                            <div class="col-md-6">
                                <select class="form-control" name="user_id">
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">
                                            {{$user->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

        </td>
            <input type = "submit" name = "submit" value = "Save Interview">
        </div>
    </form>
@endsection;