<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            'text' => 'first interview summary',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('interviews')->insert([
            'text' => 'second interview summary',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
